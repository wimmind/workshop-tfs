import {
  simpleAnimate,
  simpleAnimateWithCallBack,
  simpleAnimatePromisified
} from "./simpleAnimate.js";

const getBoxesIn = wrapper =>
  [".js-box-red", ".js-box-coral", ".js-box-blue"].map(
    wrapper.querySelector.bind(wrapper)
  );
const parallelAnimate = boxes => boxes.forEach(simpleAnimate);

const cases = [
    parallelAnimate,
    // здесь добавляем остальные кейсы
];

cases.forEach((runCase, caseIndex) => {
  const wrapper = document.querySelectorAll(".js-case")[caseIndex];
  const startButton = wrapper.querySelector(".js-start-button");
  startButton.addEventListener("click", () => runCase(getBoxesIn(wrapper)));
});
