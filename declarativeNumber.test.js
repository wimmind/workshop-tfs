const DeclarativeNumber = require("./declarativeNumber");

describe("DeclarativeNumber", () => {
  it("базовый кейс", () => {
    const actual =
      new DeclarativeNumber(5)
        .add(5)
        .minus(2)
        .multiply(3)
        .devide(2) + 2;
    const expected = (5 + 5 - 2) * 3 / 2 + 2;
    expect(actual).toBe(expected);
  });
  xit("не храним состояние", () => {
    const five = new DeclarativeNumber(5);
    const seven = five.add(2);
    expect(seven.valueOf()).toBe(7);
    expect(five.valueOf()).toBe(5);
  });
  xit("может аргументом принимать собственный тип", () => {
    const five = new DeclarativeNumber(new DeclarativeNumber(new DeclarativeNumber(5)));
    const ten = new DeclarativeNumber(5).add(five);
    expect(five.valueOf()).toBe(5);
    expect(ten.valueOf()).toBe(10);
  });
  xit('с методом executeWithValue', () => {
    const five = new DeclarativeNumber(new DeclarativeNumber(new DeclarativeNumber(5)));
    const seven = five.executeWithValue(value  =>  value + 1).executeWithValue(value => value + 1);
    expect(five.valueOf()).toBe(5);
    expect(seven.valueOf()).toBe(7);
  })
});
